package Person;

public class Person {
    private String personName;
    private int personHeight;
    private String personGender;
    private String personHometown;


    public Person(String personName, int personHeight, String personGender, String personHometown) {
        this.personName = personName;
        this.personHeight = personHeight;
        this.personGender = personGender;
        this.personHometown = personHometown;
    }

    public String introduction() {
        return "Greetings Everyone! My name is " + personName + " and my hometown is " + personHometown + ".";
    }

    public String comparingHometowns(Person comparingPerson) {
        if (comparingPerson.personHometown.equals(personHometown)) {
            return "I know you";
        } else return introduction();
    }

    final double inch = 0.394;

    public String getHeightInches() {
        return "Your Height is " + personHeight * inch + " Inches, or (" + personHeight + ") in real world units..cm";
    }
}