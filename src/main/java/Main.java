import Person.Person;

public class Main {

    public static void main(String[] args) {
        Person jerry = new Person("Jerry", 185, "Male", "Strängnäs");
        Person leif = new Person("Leif", 180, "Male", "Eskilstuna");
        Person klas = new Person("Klas", 165, "Male", "Strängnäs");

        System.out.println(jerry.introduction());
        System.out.println(jerry.getHeightInches());

        System.out.println(jerry.comparingHometowns(leif));
        System.out.println(jerry.comparingHometowns(klas));

    }
}
